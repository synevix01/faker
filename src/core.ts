/**
 * 
 * @param data An object describing the data structure
 * @returns 
 */
export function faker<T>(data: T) {

    const result = illustrate(data);
 
    return result;
}



/**
 * This function helps to repeat a certain data structure
 * @param data 
 * @param options 
 * @param meta An additional information added to the options
 * @returns 
 */
export function repeat<T>(data: T, options: IOptions<T>, meta?: T): any {

    const dataKey = Object.keys(data)
    const dataValues = Object.values(data);

    let holder = {};

    for (let i = 0; i < options.count; i++) {
        let key = data[options.key] as any;

        if (typeof key === 'function') {
            key = key();
        } else {
            key = key + i.toString()
            console.warn('Key provided is auto generate')
        }

        const container = {};

        dataKey.forEach((element, index) => {

            if (meta && meta[element]) {
                container['$Extra_Meta_Data$'] = meta
            }

            if (!dataKey.includes(options.key.toString())) {
                throw new Error("Invalid key specified");
            }

            container[element] = dataValues[index]
            container['$Unique_Primary_Key$'] = options.key

        });

        holder[i] = container
    }

    return holder
}




const illustrate = <T>(data: T) => {


    const values = Object.values(data);

    let holder = {};

    values.forEach((item) => {
        const container = {}
        const keys1 = Object.keys(item)
        const values1 = Object.values(item);

        const uniquePrimaryKey = item['$Unique_Primary_Key$'];
        const extraMetaData = item['$Extra_Meta_Data$'];
        const metaKeys = extraMetaData && Object.keys(extraMetaData)


        values1.forEach((element, ic) => {


            if (typeof element === 'function') {

                if (metaKeys && metaKeys.includes(keys1[ic])) {
                    const inputs = extraMetaData[keys1[ic]];
                    container[keys1[ic]] = element(inputs)
                } else {
                    container[keys1[ic]] = element()
                }

            } else {
                if (keys1[ic] === '$Unique_Primary_Key$') {
                    return
                }
                if (keys1[ic] === '$Extra_Meta_Data$') {
                    return
                }
                if (typeof element === 'string') {
                    container[keys1[ic]] = element
                    return
                }

                container[keys1[ic]] = illustrate(element)
            }

        });

        

        holder[container[uniquePrimaryKey]] = container


    })



    return holder;
}


export interface IOptions<T> {
    count?: number
    key?: keyof T;
}
