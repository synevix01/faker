/**
 * Get a random username
 * @returns 
 */
export const getUsername = () => {
    const username = ['eramaz', 'reasre', 'joease', 'precious', 'unirams', 'tampores', 'verak', 'joezy',
        'rhoadia', 'kirencia', 'luckie', 'johnny', 'grammly', 'brightz', 'docty', 'ricky', 'omans', 'milky',
        'nini', 'vanesek', 'helmet', 'barbs', 'veribos', 'sandics', 'sammy', 'quenni', 'opran', 'titi', 'ginak'];

    const index = getRandomNumber(username.length);

    return username[index];
}


/**
 * Get a random email address
 * @returns 
 */
export const getEmail = () => {
    const email = ['erama@gmail.com', 'reasre@hotmail.com', 'joease@gmail.com', 'precious@yahoo.com',
        'unirams@urv.co', 'tampores@rex.com', 'verak@msn.com', 'kirencia@deco.com', 'grimys@hotmail.com',
        'ejar@gmail.com', 'weased@ast.co.uk', 'barbe@gmail.com', 'viraps@hotmail.com', 'zinca@yahoo.com',
        'authum@eas.com', 'salty@space.com', 'gerine@msn.com', 'fressh@jre.biz'];

    const index = getRandomNumber(email.length);

    return email[index];
}


/**
 * Get a random name
 * @returns 
 */
export const getName = () => {
    const name = ['Grams Henry', 'John Doe', 'Emmanuella Asamoah', 'Philip Osei', 'Angela Oppong Ansah',
        'Priscilla Fosu', 'Elsie Abbey', 'Moses Oduro', 'Lawrencia Appiah', 'Felix Osei', 'Victor Mins',
        'Nini Larms', 'Joshua Ford', 'Zach Rosen', 'Mary Basset', 'Sofia Wylie', 'Frankie Cyr', 'Julie Kin',
        'Ann Mann', 'Bryan Rey', 'Lex Hogan', 'Litah Ricks'];

    const index = getRandomNumber(name.length);

    return name[index];
}

/**
 * Get a random unique id
 * @returns 
 */
export const getId = (id?: string) => {
    const random = Math.random().toString(36).substring(2) + Math.random().toString(36).substring(2);

    if (!id) {
        return random;
    }

    if (!global[id]) {
        global[id] = random
    }

    return global[id]

}

/**
 * Get a random contact
 * @returns 
 */
export const getContact = () => {
    const contact = ['0240000000', '0203849583', '0279485743', '0249203943', '0240923945',
        '0233234324', '0208576341', '0259679454', '0224543267', '0546540898', '0558743254',
        '0242132432', '0267969543', '0207965786', '0557263476', '0569574832', '0238967543',
        '0205668755', '0249867190', '0201856398', '0234576845', '0559812543', '0553216895'];

    const index = getRandomNumber(contact.length);

    return contact[index];

}


/**
 * Get a random image url
 * @returns 
 */
export const getImage = () => {
    const image = [
        'https://outfittrends.b-cdn.net/wp-content/uploads/2021/03/D5_PADGWAAAdSmO-400x500.jpeg',
        'https://i.pinimg.com/originals/9d/f8/fe/9df8fe92add624b7a4cfb59945502233.jpg',
        'https://d17a17kld06uk8.cloudfront.net/products/43VMDHH/CH3TJT57-default.jpg',
        'https://theglossychic.com/wp-content/uploads/2019/11/20191110_001641.jpg',
        'https://i.pinimg.com/564x/82/92/b5/8292b56c3eb975ab9356b85eb1160716.jpg',
        'https://i.pinimg.com/736x/6d/34/9c/6d349c286bfb9fc1dc4863590278545b.jpg',
        'https://global2019-static-cdn.kikuu.com/upload-productImg-1583384640756.jpeg?x-oss-process=style/p85'
    ];

    const index = getRandomNumber(image.length);

    return image[index];

}

/**
 * Get a random text
 * @returns 
 */
export const getText = () => {
    const text = [
        'I really love you',
        'What have you been up to',
        'My name is Decimalvalues',
        'You can call me anytime',
        'Please where do you stay',
        'There is something I want to tell you',
        'You just have to find something that makes you happy',
        'There is always a new way of doing things',
        'I think travelling is always the best option to avoid boredom',
        'What will be your perfect choice of colors if given the opportunity',
        'Whispering is bad for the voice, so it\'s kinda very, very quiet',
        'So I can put on my best face whenever it rains again',
        'She wants me to tell her how I feel',
        'I believe the word he used was forever',
        'We are coming now',
        'I need someone to take a picture and send it over to me',
        'Who do you want to be with',
        'We sometimes get ourselves worried for nothing',
        'If there were more wishes available',
        'Someone can decide to build an entire empire with a space of time',
        'Giving yourself a break or space literally calls for a heart break',
        'How sure are you that they can arrive on time',
        'A certain man spent a 100 years spying on a woman'
    ];

    const index = getRandomNumber(text.length);

    return text[index];

}


/**
 * Get a random number with a given count number
 * @param count The number of decimal places in hundredth
 * @returns 
 */
export const getRandomNumber = (count: number) => {

    return Math.floor(Math.random() * count);
}


/**
 * Get a random element from the given list
 * @param args 
 * @returns 
 */
export const getFromList = (args: number[] | string[]) => {

    const randomNumber = getRandomNumber(args.length);

    return args[randomNumber]

}

/**
 * Get a random true or false value
 * @returns 
 */
export const getBoolean = () => {

    const index = getRandomNumber(2);

    return index ? true : false
}


/**
 * Get a random number from a given range
 * @param from The minimum number
 * @param to The maximum number
 * @returns 
 */
export const getNumber = (from: number | number[], to?: number) => {

    let start = null;
    let end = null;
    let counter = 0;


    const container: number[] = [];

    if (Array.isArray(from)) {
        start = from[0];
        end = from[1] && from[1]

    } else {
        start = from
        end = to
    }

    if (typeof end === 'undefined') {
        end = start;
        start = 0;
    }


    while (counter < end) {

        container.push(counter);
        counter = counter + 1
    }

    const index = getRandomNumber(container.length);

    return container[index].toString();
}


/**
 * Get a random gender, either male or female
 * @returns 
 */
export const getGender = () => {
    const index = getRandomNumber(2);

    return index ? 'Male' : 'Female'

}


/**
 * Get a random country
 * @returns 
 */
export const getCountry = () => {

    const country = [
        'America', 'Argentina', 'Angola', 'Australlia',
        'Benin', 'Brazil', 'Burkina Faso',
        'Canada', 'Chad', 'China',
        'Denmark',
        'England',
        'France',
        'Gambia', 'Germany', 'Ghana',
        'Italy',
        'Japan',
        'Mali', 'Morocco', 'Mexico',
        'Nigeria', 'Niger',
        'Portugual',
        'Qatar',
        'Russia',
        'Spain', 'South Africa',
        'Togo', 'Turkey',
        'United Kingdom',
        'Vietman',
        'Zambia'

    ];

    const index = getRandomNumber(country.length);

    return country[index];
}



/**
 * Get a random color
 * @returns 
 */
export const getColor = () => {

    const color = [
        'Red', 'Blue', 'Orange', 'White', 'Yellow', 'Black', 'Violet', 'Indigo', 'Gray',
        'Green', 'Brown', 'Cyan', 'Purple', 'Pink'

    ];

    const index = getRandomNumber(color.length);

    return color[index];
}


/**
 * Get a random title
 * @returns 
 */
 export const getTitle = () => {

    const title = [
        'Mr', 'Mrs', 'Mss', 'Dr', 'Prof'

    ];

    const index = getRandomNumber(title.length);

    return title[index];
}