import { createWriteStream } from "fs";
import { assocPath, lensPath, omit, over, view } from 'ramda'
import { pathParser } from "./utils";
//import { EventEmitter } from 'events'

//const event = new EventEmitter()

/**
 *  The set function is used to insert new data into the faker database
 * @param path The url path
 * @param data The data payload 
 */
export const set = async (path: string, data: any) => {

    try {
        const fetching = await fetch(path,
            {
                method: 'post',
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json'
                },
                body: JSON.stringify(data)
            });
        const response = await fetching.json();
        return response;
    }
    catch (e) {
        throw new Error(e);
    }

}




/**
 *  The set function is used to update  data in the faker database
 * @param path The file path to set new data to 
 * @param data The data payload
 * @param onComplete Returns a callback after success or failure
 */
export const update = async (path: string, data: any, onComplete?: (error: Error) => void) => {

    // const { basePath, reference } = pathParser(path);

    // const location = reference.split('/')
    // location.shift();

    // let file = null;

    // try {
    //     file = await import(basePath);
    // } catch (error) {
    //     file = {}
    // }

    // const newData = assocPath(location, data, file)
    // event.emit('valueChange')
    // createWriteStream(basePath, { encoding: 'utf-8', autoClose: true }).write(JSON.stringify(newData), (error) => onComplete(error))

}



/**
 *  The set function is used to remove data from the faker database
 * @param path The file path to set new data to
 * @param onComplete Returns a callback after success or failure
 */
export const remove = async (path: string, onComplete?: (error: Error) => void) => {

    // const { basePath, reference } = pathParser(path);

    // const location = reference.split('/')
    // location.shift();

    // let file = null;

    // try {
    //     file = await import(basePath);
    // } catch (error) {
    //     file = {}
    // }


    // const pred = (item: any) => {
    //     return null
    // }

    // event.emit('valueChange')
    // const newData = over(lensPath(location), pred, file);

    // createWriteStream(path, { encoding: 'utf-8', autoClose: true }).write(JSON.stringify(newData), (error) => onComplete(error))

}


/**
 * The set function is used to get  data from the faker database
 * @param path The file path to set new data to
 * @param onComplete Returns a callback after success or failure
 */
export const get = async <T>(path: string) => {

    const { basePath, reference } = pathParser(path);

    const location = reference.split('/')
    location.shift();

    let file = null;

    try {
        const fetching = await fetch(basePath);
        file = await fetching.json();
    } catch (e) {
        throw new Error(e);
    }

    const newData = view(lensPath(location), file) as T;

    return newData


}


/**
 * The set function is used to get  data from the faker database and listens to new data change
 * @param path The file path to set new data to
 * @param onComplete Returns a callback after success or failure
 */
export const onValue = async (path: string, onComplete?: (response: any) => void) => {

    // event.on('valueChange', () => {
    //     get(path, onComplete)
    // })
}