import { argv, cwd } from 'process'
import { resolve, join, parse } from 'path'
import { existsSync, mkdir, createWriteStream } from 'fs';
import express = require('express');
import bodyParser = require('body-parser');
import { assocPath } from 'ramda';


const configure = async () => {

    if (argv.length < 3) {
        throw new Error("Argument is incomplete. Add the configuration file");
    }

    const filename = argv[2];
    const basePath = cwd();

    const configPath = resolve(basePath, filename);

    const file: FakerConfig = await import(configPath)

    if (Array.isArray(file)) {

        file.forEach(element => {
            injuction(element, basePath)
        });

        return;
    }


    injuction(file, basePath)

}

const injuction = (file: FakerConfig, basePath: string) => {
    const { output, structure, filename, server } = file


    const dir = join(basePath, output)
    let cloneBasePath = basePath;


    if (!existsSync(dir)) {
        const subDir = output.split('/')
        subDir.forEach(i => {
            const subPath = join(basePath, i)
            mkdir(subPath, () => null)
            cloneBasePath = subPath;
        })

    }

    if (structure || filename) {
        const path = join(basePath, output, filename)

        const { ext, name } = parse(path)

        let dataFormat = JSON.stringify(structure);

        

        createWriteStream(path, { encoding: 'utf-8', autoClose: true }).write(dataFormat, () => null)
    }

    if (server.start) {
        const app = express();
        app.use(bodyParser.urlencoded({ extended: false }));
        app.use(bodyParser.json())

        app.get(`/${output}/*`, function (req, res) {
            res.sendFile(resolve(output, req.params[0] + '.json'));
        });

        app.post(`/${output}/*`, async function (req, res) {
            const params = (req.params[0]).split('/')
            const filename = params[0];
            const path = join(basePath, output, filename);


            try {
                file = await import(path); 
            } catch (error) {
                file = {}
                console.log(error)
            }

            params.shift()

            const newData = assocPath(params, req.body, file)

            createWriteStream(path, { encoding: 'utf-8', autoClose: true }).write(JSON.stringify(newData), (error) => {
                res.json({message: 'Saved Succesfully'});
            })

           

        });


        app.listen(server.port, function () {
            console.log(`Server started at http://127.0.0.1:${server.port}`);
        });



    }

}


export default configure

export interface FakerConfig {
    output?: string;
    filename?: string;
    structure?: unknown;
    server?: {
        start: boolean,
        port: number
    }
}