// import { existsSync, writeFile } from "fs"
// import { join } from "path"
// import { cwd } from "process"

// export const createFileIfNotExist = (path: string) => {

//     if (!existsSync(path)) {
//         writeFile(path, '', () => null)
//     }
// }


export const pathParser = (path: string) => {

    const originalPath = path.split('.json/');
    const basePath = originalPath[0];
    const reference = originalPath[1];
    return { basePath, reference };
}


// export const isFileExist = (path: string) => {

//     if (!existsSync(path)) {
//         return false;    }
//     return true
// }