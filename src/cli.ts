#!/usr/bin/env node

import configure from './configure';

configure();

export { FakerConfig } from './configure'