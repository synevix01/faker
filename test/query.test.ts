
import { get, onValue, remove, set, update } from "../dist"
const { getContact, getId, getName, getNumber, getUsername } = require('../dist');

test('value change', async () => {

    // await onValue('faker/gallery.json/84ocgo3x7bbn71qz11pg7b', (res) => {

    // })

    expect(2).toBe(2)
})

test('set', async () => {

    const data = {
        "uid": getId('one'),
        "username": getUsername(),
        "src": {
            [getId('two')]: {
                "uid": getId('two'),
                "name": getName(),
                "contact": {
                    [getId('three')]: {
                        "uid": getId('three'),
                        "phoneNumber": getContact(),
                        "age": getNumber(2, 16),
                        "size": getNumber(10, 30)
                    }
                }
            }
        }
    }


    //await set(`faker/gallery.json/${getId('one')}`, data, () => null)

    expect(2).toBe(2)
})


test('update', async () => {

    const data = 'Elsie Aikins';

    //await update('faker/gallery.json/84ocgo3x7bbn71qz11pg7b/username', data, () => null)

    expect(2).toBe(2)
})


test('remove', async () => {

    //await remove('faker/gallery.json/84ocgo3x7bbn71qz11pg7b/username', () => null)

    expect(2).toBe(2)
})

test('get', async () => {
    
   // await get('http://127.0.0.1:19000/assets/faker/gallery.json/84ocgo3x7bbn71qz11pg7b', () => null)

    expect(2).toBe(2)
})


