import {
    getUsername, getContact, getEmail, getName, getText,
    getFromList, getId, getImage, getBoolean, getNumber, getColor, getCountry, getGender, getTitle
} from "../src/utils"

test('getUsername', () => {
    const username = getUsername()
    expect(username).not.toBeNull()
})

test('getEmail', () => {
    const email = getEmail()
    expect(email).not.toBeNull()
})

test('getContact', () => {
    const contact = getContact()
    expect(contact).not.toBeNull()
})

test('getText', () => {
    const text = getText()
    expect(text).not.toBeNull()
})

test('getName', () => {
    const name = getName()
    expect(name).not.toBeNull()
})

test('getFromList', () => {
    const randomRange = getFromList([1, 2, 3, 4, 5])
    expect(randomRange).not.toBeNull()
})

test('getId', () => {
    const id = getId()
    expect(id).not.toBeNull()
})

test('getImage', () => {
    const image = getImage()
    expect(image).not.toBeNull()
})

test('getBoolean', () => {
    const boolean = getBoolean()
    expect(boolean).not.toBeNull()
})

test('getNumber', () => {
    const number = getNumber(10, 20)
    expect(number).not.toBeNull()
})

test('getTitle', () => {
    const number = getTitle()
    expect(number).not.toBeNull()
})

test('getCountry', () => {
    const number = getCountry()
    expect(number).not.toBeNull()
})

test('getColor', () => {
    const number = getColor()
    expect(number).not.toBeNull()
})

test('getGender', () => {
    const number = getGender()
    expect(number).not.toBeNull()
})