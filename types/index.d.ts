export { faker, repeat } from './core';
export { getContact, getEmail, getId, getImage, getName, getFromList, getText, getUsername, getBoolean, getNumber, getColor, getCountry, getGender, getTitle } from './lib';
export { get, onValue, remove, set, update } from './query';
//# sourceMappingURL=index.d.ts.map