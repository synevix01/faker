/**
 *
 * @param data An object describing the data structure
 * @returns
 */
export declare function faker<T>(data: T): {};
/**
 * This function helps to repeat a certain data structure
 * @param data
 * @param options
 * @param meta An additional information added to the options
 * @returns
 */
export declare function repeat<T>(data: T, options: IOptions<T>, meta?: T): any;
export interface IOptions<T> {
    count?: number;
    key?: keyof T;
}
//# sourceMappingURL=core.d.ts.map