/**
 *  The set function is used to insert new data into the faker database
 * @param path The url path
 * @param data The data payload
 */
export declare const set: (path: string, data: any) => Promise<any>;
/**
 *  The set function is used to update  data in the faker database
 * @param path The file path to set new data to
 * @param data The data payload
 * @param onComplete Returns a callback after success or failure
 */
export declare const update: (path: string, data: any, onComplete?: (error: Error) => void) => Promise<void>;
/**
 *  The set function is used to remove data from the faker database
 * @param path The file path to set new data to
 * @param onComplete Returns a callback after success or failure
 */
export declare const remove: (path: string, onComplete?: (error: Error) => void) => Promise<void>;
/**
 * The set function is used to get  data from the faker database
 * @param path The file path to set new data to
 * @param onComplete Returns a callback after success or failure
 */
export declare const get: <T>(path: string) => Promise<T>;
/**
 * The set function is used to get  data from the faker database and listens to new data change
 * @param path The file path to set new data to
 * @param onComplete Returns a callback after success or failure
 */
export declare const onValue: (path: string, onComplete?: (response: any) => void) => Promise<void>;
//# sourceMappingURL=query.d.ts.map