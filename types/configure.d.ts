declare const configure: () => Promise<void>;
export default configure;
export interface FakerConfig {
    output?: string;
    filename?: string;
    structure?: unknown;
    server?: {
        start: boolean;
        port: number;
    };
}
//# sourceMappingURL=configure.d.ts.map