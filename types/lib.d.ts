/**
 * Get a random username
 * @returns
 */
export declare const getUsername: () => string;
/**
 * Get a random email address
 * @returns
 */
export declare const getEmail: () => string;
/**
 * Get a random name
 * @returns
 */
export declare const getName: () => string;
/**
 * Get a random unique id
 * @returns
 */
export declare const getId: (id?: string) => any;
/**
 * Get a random contact
 * @returns
 */
export declare const getContact: () => string;
/**
 * Get a random image url
 * @returns
 */
export declare const getImage: () => string;
/**
 * Get a random text
 * @returns
 */
export declare const getText: () => string;
/**
 * Get a random number with a given count number
 * @param count The number of decimal places in hundredth
 * @returns
 */
export declare const getRandomNumber: (count: number) => number;
/**
 * Get a random element from the given list
 * @param args
 * @returns
 */
export declare const getFromList: (args: number[] | string[]) => string | number;
/**
 * Get a random true or false value
 * @returns
 */
export declare const getBoolean: () => boolean;
/**
 * Get a random number from a given range
 * @param from The minimum number
 * @param to The maximum number
 * @returns
 */
export declare const getNumber: (from: number | number[], to?: number) => string;
/**
 * Get a random gender, either male or female
 * @returns
 */
export declare const getGender: () => "Male" | "Female";
/**
 * Get a random country
 * @returns
 */
export declare const getCountry: () => string;
/**
 * Get a random color
 * @returns
 */
export declare const getColor: () => string;
/**
 * Get a random title
 * @returns
 */
export declare const getTitle: () => string;
//# sourceMappingURL=lib.d.ts.map