# Decimalvalues Faker

## Table of contents

1. [Introduction](#introduction)
2. [Installation](#paragraph1)
3. [Usage](#paragraph2)
4. [Configuration](#paragraph3)
5. [API Reference](#paragraph4)
6. [Example](#paragraph5)
7. [Contributing](#paragraph6)
8. [Feedback](#paragraph7)
9. [License](#paragraph8)
10. [Authors](#paragraph8)

## Introduction

Decimalvalues Faker is a tiny javascript library for creating fake random data. It comes with its own command line to help in the creating of the fake data.

## Installation

```bash
    npm install --save-dev @decimalvalues/faker
```

## Usage

Add the following line to the package script property

```json
    "scripts": {
        "faker": "decimalvalues-faker  faker.config.js"
    }
```

Create a javascript file at the root of the application with any name eg "faker.config.js"

```bash
    npm run faker
```

## Configuration

An object module with the following properties:

| Properties  | Description                                                                  |
| :---------- | :--------------------------------------------------------------------------- |
| `outDir`    | The output directory for the random faker data.                              |
| `filename`  | The name to be given to the random data.                                     |
| `structure` | This property contains an abstract construction of the faker data structure. |

## API Reference

| Functions     | Description                                              |
| :------------ | :------------------------------------------------------- |
| `getUsername` | Generate random username                                 |
| `getEmail`    | Generate random email                                    |
| `getId`       | Generate a unique id                                     |
| `getText`     | Generate random text                                     |
| `getName`     | Generate random full name                                |
| `getContact`  | Generate random contact                                  |
| `getImage`    | Generate random image link                               |
| `getFromList` | Generate a random content based on the given argument    |
| `getBoolean`  | Generate a random boolean                                |
| `getNumber`   | Generate a random number from a range of input           |
| `getCountry`  | Generate a random country                                |
| `getGender`   | Generate a random gender                                 |
| `getColor`    | Generate a random color                                  |
| `getTitle`    | Generate a random title `eg` Mr                          |
| `faker`       | A generic function that wraps the data structure         |
| `repeat`      | A function that helps to repeat a certain data structure |

## Example

faker.config.js

```javascript
const {
  faker,
  getUsername,
  getId,
  repeat,
  getName,
  getEmail,
} = require("@decimalvales/faker");

/** @type {import("./types").FakerConfig} */
module.exports = {
  outDir: "/",
  filename: "data.json",
  structure: faker({
    username: getUsername(),
    id: getId(),
    friends: repeat(
      {
        fullName: getName,
        email: getEmail,
        id: getId,
      },
      { count: 2, key: "id" }
    ),
  }),
};
```

## Contributing

Contributions are always welcome!

See `contributing.md` for ways to get started.

Please adhere to this project's `code of conduct`.

## Feedback

If you have any feedback, please reach out to us at decimalvalues@gmail.com

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Authors

- [@Emmanuella Asamoah](https://www.github.com/easamoah88)
- [@Henry Asiedu](https://www.github.com/docty)

## Repository

[Decimalvalues-faker](https://gitlab.com/synevix01/faker)
